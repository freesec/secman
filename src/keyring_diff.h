/**
* 	secman -- A command-line SECret MANager.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2015-2020 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KEYRING_DIFF_H_
#define KEYRING_DIFF_H_
#include <gnome-keyring-1/gnome-keyring.h>
typedef struct {
	GList * k1_minus_k2_secrets;
	GList * k1_inter_k2_same_value_secrets;
	GList * k1_inter_k2_diff_value_secrets;
	GList * k2_minus_k1_secrets;

} diff_res;

typedef enum {
	K1_MINUS_K2, K2_MINUS_K1, K1_INTER_K2_DIFF, K1_INTER_K2_EQ
} DIFF_RES_SET;

void diff_dr(const gchar* keyring1, const gchar* keyring2, diff_res* dr);
void print_diff_res_subset(diff_res * dr, DIFF_RES_SET printed_set);
void diff_res_set(diff_res *dr, DIFF_RES_SET set_id, GList* new_set);
guint copy_dr_secrets(const gchar* out_keyring, diff_res* dr);
#endif /* KEYRING_DIFF_H_ */
