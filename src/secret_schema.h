/**
* 	secman -- A command-line SECret MANager.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2015-2020 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

/*
 * pwd_schema.h
 *
 *  Created on: Feb 3, 2015
 */

#ifndef SEC_SCHEMA_H_
#define SEC_SCHEMA_H
extern GnomeKeyringPasswordSchema sec_schema;
#endif /* SEC_SCHEMA_H_ */
