/**
* 	secman -- A command-line SECret MANager.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2015-2020 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include "keyring_diff.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <secret_schema.h>

typedef struct {
	char *sec_name;
	char *sec_value1;
	char *sec_value2;
} secret;

GList* diff_res_get(diff_res *dr, DIFF_RES_SET set_id) {
	GList* set = NULL;
	switch (set_id) {
	case K1_MINUS_K2:
		set = dr->k1_minus_k2_secrets;
		break;
	case K2_MINUS_K1:
		set = dr->k2_minus_k1_secrets;
		break;
	case K1_INTER_K2_DIFF:
		set = dr->k1_inter_k2_diff_value_secrets;
		break;
	case K1_INTER_K2_EQ:
		set = dr->k1_inter_k2_same_value_secrets;
		break;
	}
	return set;
}

GList* create_secret_list(secret * s) {
	GList* l = malloc(sizeof(GList));
	l->data = s;
	l->prev = NULL;
	l->next = NULL;
	return l;
}

void diff_res_set(diff_res *dr, DIFF_RES_SET set_id, GList* new_set) {
	switch (set_id) {
	case K1_MINUS_K2:
		dr->k1_minus_k2_secrets = new_set;
		break;
	case K2_MINUS_K1:
		dr->k2_minus_k1_secrets = new_set;
		break;
	case K1_INTER_K2_DIFF:
		dr->k1_inter_k2_diff_value_secrets = new_set;
		break;
	case K1_INTER_K2_EQ:
		dr->k1_inter_k2_same_value_secrets = new_set;
		break;
	}
}

secret* create_secret_1v(char * sec_name, char* sec_value) {
	secret * s = malloc(sizeof(secret));
	s->sec_value1 = sec_value;
	s->sec_value2 = NULL;
	s->sec_name = sec_name;
	return s;
}

secret* create_secret_2v(char * sec_name, char* sec_value1, char* sec_value2) {
	secret * s = malloc(sizeof(secret));
	s->sec_value1 = sec_value1;
	s->sec_value2 = sec_value2;
	s->sec_name = sec_name;
	return s;
}

secret* extract_secret(const gchar* keyring, guint32 s_id) {
	gchar * sec_name, *sec_value;
	GnomeKeyringItemInfo * info;
	assert(
			gnome_keyring_item_get_info_sync(keyring, s_id, &info)
					== GNOME_KEYRING_RESULT_OK);
	sec_name = gnome_keyring_item_info_get_display_name(info);
	sec_value = gnome_keyring_item_info_get_secret(info);
	return create_secret_1v(sec_name, sec_value);
	gnome_keyring_item_info_free(info);
	return NULL;
}

void diff_res_insert_first(diff_res *dr, DIFF_RES_SET set_id, secret* s) {
	GList* set = diff_res_get(dr, set_id);
	GList* link = create_secret_list(s);
	if (set) {
		link->next = set;
		set->prev = link;

	}
	diff_res_set(dr, set_id, link);
}

void add_list_in_dr(const gchar* keyring2, GList* sec_id_list, diff_res* dr,
		DIFF_RES_SET add_set) {
	while (sec_id_list != NULL) {
		diff_res_insert_first(dr, add_set,
				extract_secret(keyring2, GPOINTER_TO_UINT(sec_id_list->data)));
		sec_id_list = sec_id_list->next;
	}
}

GList* del_list_elmt(GList* elmt, GList* list) {
	if (elmt->prev != NULL) {
		elmt->prev->next = elmt->next;
		if (elmt->next != NULL)
			elmt->next->prev = elmt->prev;
	} else if (elmt->next != NULL) {
		list = elmt->next;
		if (elmt->next != NULL)
			elmt->next->prev = NULL;
	}
	else
		list = NULL;
	return list;
}

void print_diff_res_subset(diff_res * dr, DIFF_RES_SET printed_set) {
	GList* link = diff_res_get(dr, printed_set);
	while (link != NULL) {
		printf("%s ", ((secret*) link->data)->sec_name);
		link = link->next;
	}
	printf("\n");
}

void print_diff_res(diff_res * dr, const gchar* k1, const gchar* k2) {
	printf("[K1_ONLY] Secret(s) only in keyring %s : ", k1);
	print_diff_res_subset(dr, K1_MINUS_K2);
	printf("[K2_ONLY] Secret(s) only in keyring %s : ", k2);
	print_diff_res_subset(dr, K2_MINUS_K1);
	printf(
			"[K1_AND_K2_EQ] Secret(s) in both keyrings %s and %s, same value(s) : ",
			k1, k2);
	print_diff_res_subset(dr, K1_INTER_K2_EQ);
	printf(
			"[K1_AND_K2_DIFF] Secret(s) in both keyrings %s and %s, different value(s) : ",
			k1, k2);
	print_diff_res_subset(dr, K1_INTER_K2_DIFF);
}

void diff_dr(const gchar* keyring1, const gchar* keyring2, diff_res* dr) {
	char *k1_sec_name, *k2_sec_name, *k1_sec_value, *k2_sec_value;
	GnomeKeyringItemInfo * info1, *info2;
	GList * ids1, *ids2, *cur1, *cur2;
	assert(
			gnome_keyring_list_item_ids_sync(keyring1, &ids1)
					== GNOME_KEYRING_RESULT_OK);
	assert(
			gnome_keyring_list_item_ids_sync(keyring2, &ids2)
					== GNOME_KEYRING_RESULT_OK);
	cur1 = ids1, *cur2;
	while (cur1 != NULL) {
		assert(
				gnome_keyring_item_get_info_sync(keyring1, GPOINTER_TO_UINT(cur1->data), &info1) == GNOME_KEYRING_RESULT_OK);
		k1_sec_name = gnome_keyring_item_info_get_display_name(info1);
//			printf("k1 secret %s \n", k1_sec_name);
		cur2 = ids2;
		while (cur2 != NULL) {
			assert(
					gnome_keyring_item_get_info_sync(keyring2, GPOINTER_TO_UINT(cur2->data), &info2) == GNOME_KEYRING_RESULT_OK);
			k2_sec_name = gnome_keyring_item_info_get_display_name(info2);
//				printf("k2 secret %s \n", k2_sec_name);
			if (!strcmp(k1_sec_name, k2_sec_name)) {
				//delete cur2 from ids2 (in order to let only K2_MINUS_K1 elements in ids2)
				ids2 = del_list_elmt(cur2, ids2);
				k1_sec_value = gnome_keyring_item_info_get_secret(info1);
				k2_sec_value = gnome_keyring_item_info_get_secret(info2);
				if (strcmp(k1_sec_value, k2_sec_value))
					diff_res_insert_first(dr, K1_INTER_K2_DIFF,
							create_secret_2v(k1_sec_name, k1_sec_value,
									k2_sec_value));
				else
					diff_res_insert_first(dr, K1_INTER_K2_EQ,
							create_secret_1v(k1_sec_name, k1_sec_value));
				break;
			}
			cur2 = cur2->next;
		}
		cur1 = cur1->next;
		if (cur2 == NULL)  //intersection k1,k2 didn't occur
			//k1_sec_name belongs to K1_MINUS_K2
			diff_res_insert_first(dr, K1_MINUS_K2,
					create_secret_1v(k1_sec_name, k1_sec_value));
	}
	//K2_MINUS_K1 subset maps ids2 list
	add_list_in_dr(keyring2, ids2, dr, K2_MINUS_K1);
}

GnomeKeyringResult copy_secret(const gchar* out_keyring, secret* s) {
	GnomeKeyringResult r;
	const gchar* value;
	value = s->sec_value1;
	if (s->sec_value2 != NULL) {
		char c = 0;
		int ret = 0;
		while (c != '1' && c != '2') {
			printf("Secret %s, values : %s (1), %s (2) : ", s->sec_name,
					s->sec_value1, s->sec_value2);
			ret = scanf("%c", &c);
			if (c == '\n')
				ret = scanf("%c", &c);
			if (ret != 1)
				c = 0;
		}
		if (c == '2')
			value = s->sec_value2;
	}
	r = gnome_keyring_store_password_sync(&sec_schema, out_keyring, s->sec_name,
			value, "id", s->sec_name, NULL);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error adding secret % to keyring : %s.\n", s->sec_name,
				gnome_keyring_result_to_message(r));
	return r;
}

guint copy_secrets(const gchar* out_keyring, GList* secrets) {
	secret* s;
	GnomeKeyringResult r;
	guint count = 0;
	while (secrets != NULL) {
		if ((s = secrets->data) != NULL) {
			r = copy_secret(out_keyring, s);
			if (r == GNOME_KEYRING_RESULT_OK)
				count++;
		}
		secrets = secrets->next;
	}
	return count;
}

guint copy_dr_secrets(const gchar* out_keyring, diff_res* dr) {
	guint count = 0;
	if (dr != NULL) {
		count += copy_secrets(out_keyring, dr->k1_minus_k2_secrets);
		count += copy_secrets(out_keyring, dr->k2_minus_k1_secrets);
		count += copy_secrets(out_keyring, dr->k1_inter_k2_same_value_secrets);
		if (dr->k1_inter_k2_diff_value_secrets != NULL) {
			printf("Two values available for at least one secret."
					" Type 1 to choose the first value, 2 for the second.\n");
			count += copy_secrets(out_keyring,
					dr->k1_inter_k2_diff_value_secrets);
		}
	}
	return count;
}
