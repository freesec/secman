/**
* 	secman -- A command-line SECret MANager.
*   Author: freesec2021 at protonmail.com
*   Copyright (C) 2015-2020 freesec (pseudonym)
*
*   This program is free software: you can redistribute it and/or modify
*   it under the terms of the GNU Affero General Public License as published
*   by the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU Affero General Public License for more details.
*
*   You should have received a copy of the GNU Affero General Public License
*   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include <gnome-keyring-1/gnome-keyring.h>
#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <secret_schema.h>
#include <time.h>
#include "keyring_diff.h"

static const int DEFAULT_AUTOLOCK_TIMEOUT = 60;
/*
 * libgnome-keyring API doc :
 * file:////usr/share/gtk-doc/html/gnome-keyring/ch01.html
 */

GnomeKeyringPasswordSchema sec_schema = { GNOME_KEYRING_ITEM_GENERIC_SECRET, { {
		"id", GNOME_KEYRING_ATTRIBUTE_TYPE_STRING }, } };

#define UNLOCK_ONE(keyring)\
		gboolean locked = is_keyring_locked(keyring);\
		if (locked)\
			unlock(keyring);

#define LOCK_ONE(keyring)\
		if (locked)\
			lock(keyring);

#define UNLOCK_TWO(keyring1,keyring2)\
		gboolean locked1 = is_keyring_locked(keyring1);\
		if (locked1)\
			unlock(keyring1);\
		gboolean locked2 = is_keyring_locked(keyring2);\
		if (locked2)\
			unlock(keyring2);

#define LOCK_TWO(keyring1,keyring2)\
		if (locked1)\
			lock(keyring1);\
		if (locked1)\
			lock(keyring1);

const gulong SECRET_DEFAULT_LEN = 100;
const gulong KEYRING_MAX_LEN = 1024;

GnomeKeyringResult get_default_keyring(gchar** deft_keyring) {
	GnomeKeyringResult r = gnome_keyring_get_default_keyring_sync(deft_keyring);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error getting the default keyring: %s.\n",
				gnome_keyring_result_to_message(r));
	return r;
}

/**
 * Checks if a keyring is set (keyring != NULL), if not returns default keyring.
 *
 * The caller must frees keyring if the returned char ptr is not NULL.
 * N.B.: use this function only in main command functions
 * (at the root of the program, one time for all ops).
 */
gchar* check_keyring(gchar* keyring) {
	gchar* printable_keyring = (gchar*) malloc(KEYRING_MAX_LEN);
	bzero(printable_keyring, KEYRING_MAX_LEN);
	GnomeKeyringResult r;
	if (!keyring) {
		r = get_default_keyring(&printable_keyring);
		if (GNOME_KEYRING_RESULT_OK != r) {
			free(printable_keyring);
			printable_keyring = NULL;
		}
		fprintf(stderr, "Using default keyring: %s.\n", printable_keyring);
	} else {
		size_t max_sz = strnlen(keyring, sizeof(KEYRING_MAX_LEN - 1));
		memcpy(printable_keyring, keyring, max_sz);
	}
	return printable_keyring;
}

void print_keyring_error(gchar* keyring, GnomeKeyringResult r,
		gchar * format_str, gboolean checking_keyring) {
	if (checking_keyring)
		keyring = check_keyring(keyring);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, format_str, keyring,
				gnome_keyring_result_to_message(r));
	if (checking_keyring)
		free(keyring);
}

void lock(const gchar *keyring) {
	keyring = check_keyring(keyring);
	GnomeKeyringResult r = gnome_keyring_lock_sync(keyring);
	if (GNOME_KEYRING_RESULT_OK != r)
		print_keyring_error(keyring, r, "Error locking keyring = %s : %s.\n",
		FALSE);
	else
		fprintf(stderr, "Keyring %s is locked.\n", keyring);
	free(keyring);
}

void lock_all() {
	GnomeKeyringResult r = gnome_keyring_lock_all_sync();
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error locking all keyrings : %s.\n",
				gnome_keyring_result_to_message(r));
	else
		fprintf(stderr, "All Keyrings are locked.\n");
}

void multi_lock(const gchar** keyrings, const guint n) {
	int i;
	for (i = 0; i < n; ++i) {
		lock(keyrings[i]);
	}
}

void unlock(const gchar *keyring) {
	keyring = check_keyring(keyring);
	GnomeKeyringResult r = gnome_keyring_unlock_sync(keyring, NULL);
	if (GNOME_KEYRING_RESULT_OK != r)
		print_keyring_error(keyring, r, "Error unlocking keyring = %s : %s.\n",
		FALSE);
	else
		fprintf(stderr, "Keyring %s is unlocked.\n", keyring);
	free(keyring);
}

void multi_unlock(const gchar** keyrings, const guint n) {
	int i;
	for (i = 0; i < n; ++i) {
		unlock(keyrings[i]);
	}
}

gboolean is_keyring_locked(const gchar* keyring) {
	GnomeKeyringInfo * info;
	GnomeKeyringResult r;
	gboolean res = TRUE;
	if (gnome_keyring_get_info_sync(keyring, &info) == GNOME_KEYRING_RESULT_OK)
		res = gnome_keyring_info_get_is_locked(info);
	else
		print_keyring_error(keyring, r, "Error opening keyring = %s : %s.\n",
		TRUE);
	gnome_keyring_info_free(info);
	return res;
}

void print_auto_lock(const gchar* keyring) {
	UNLOCK_ONE(keyring);
	GnomeKeyringInfo * info;
	GnomeKeyringResult r;
	gboolean auto_locked = FALSE;
	gboolean timeout = 0;
	if (gnome_keyring_get_info_sync(keyring, &info)
			== GNOME_KEYRING_RESULT_OK) {
		auto_locked = gnome_keyring_info_get_lock_on_idle(info);
		timeout = gnome_keyring_info_get_lock_timeout(info);
		printf("Keyring %s ", keyring);
		if (auto_locked)
			printf("is locked if idle after a timeout of %u", timeout);
		else
			printf("is not locked if idle.\n");
	} else
		print_keyring_error(keyring, r, "Error opening keyring = %s : %s.\n",
		FALSE);
	gnome_keyring_info_free(info);
	LOCK_ONE(keyring);
}

void set_auto_lock(const gchar* keyring, const gboolean auto_locked,
		const guint32 timeout) {
	keyring = check_keyring(keyring);
	UNLOCK_ONE(keyring);
	fprintf(stderr,
			"WARNING : the auto-lock functionality doesn't work"
					" and will not work until gnome-keyring daemon implements it (planned).\n");
	GnomeKeyringInfo * info;
	GnomeKeyringResult r;
	if (gnome_keyring_get_info_sync(keyring, &info)
			== GNOME_KEYRING_RESULT_OK) {
		gnome_keyring_info_set_lock_timeout(info, timeout);
		gnome_keyring_info_set_lock_on_idle(info, auto_locked);
		r = gnome_keyring_set_info_sync(keyring, info);
		if (r != GNOME_KEYRING_RESULT_OK)
			print_keyring_error(keyring, r,
					"Error setting auto-lock on keyring = %s : %s.\n", FALSE);
	} else
		fprintf(stderr, "Error opening keyring = %s : %s.\n", keyring,
				gnome_keyring_result_to_message(r));
	gnome_keyring_info_free(info);
	print_auto_lock(keyring);
	LOCK_ONE(keyring);
	free(keyring);
}

gchar* get_not_echoed_user_input(const gchar * info_str) {
	int i = -1;
	gulong alloc_len = SECRET_DEFAULT_LEN;
	gchar * input_str = malloc(alloc_len);
	struct termios oldattr, newattr;
	tcgetattr(0, &oldattr);
	newattr = oldattr;
	newattr.c_lflag &= ~ECHO;
	printf("secret (%s) : ", info_str);
	tcsetattr(0, TCSANOW, &newattr);
	do {
		input_str[++i] = getchar();
		if (i >= alloc_len - 1)
			input_str = realloc(input_str, alloc_len += 100);
	} while (input_str[i] != '\n');
	tcsetattr(0, TCSANOW, &oldattr);
	input_str[i] = '\0';
	return input_str;
}

guint32 find_secret(const gchar* keyring, const gchar * secret_name,
		GnomeKeyringItemInfo ** out_info) {
	GnomeKeyringResult r;
	gchar *tmp;
	GnomeKeyringItemInfo * info;
	GList * ids;
	guint32 id = 0;
	r = gnome_keyring_list_item_ids_sync(keyring, &ids);
	if (r != GNOME_KEYRING_RESULT_OK)
		fprintf(stderr, "Error getting secret : %s.\n",
				gnome_keyring_result_to_message(r));
	else
		while (ids != NULL) {
			r = gnome_keyring_item_get_info_sync(keyring,
					GPOINTER_TO_UINT(ids->data), &info);
			if (r != GNOME_KEYRING_RESULT_OK)
				fprintf(stderr,
						"Warning can't get info on some secret (maybe just temporary), secret id : %u,  error : %s.\n",
						GPOINTER_TO_UINT(ids->data),
						gnome_keyring_result_to_message(r));
			else {
				tmp = gnome_keyring_item_info_get_display_name(info);
				if (!strcmp(tmp, secret_name)) {
					id = GPOINTER_TO_UINT(ids->data);
					if (out_info != NULL)
						*out_info = info;
					break;
				}
				ids = ids->next;
			}
		}
	if (ids == NULL)
		fprintf(stderr,
				"Error getting secret named %s in keyring %s : not found.\n",
				secret_name, keyring);
	return id;
}

const gchar* get_secret_value(const gchar* keyring, const gchar* secret_name) {
	gchar *secret_value = NULL;
	GnomeKeyringItemInfo * info;
	guint32 id = find_secret(keyring, secret_name, &info);
	if (id > 0) {
		secret_value = gnome_keyring_item_info_get_secret(info);
	}
	return secret_value;
}

void display_secret(const gchar * keyring, const gchar * secret_name) {
	UNLOCK_ONE(keyring);
	const gchar *secret_value = get_secret_value(keyring, secret_name);
	if (secret_value != NULL) {
		printf("%s = %s\n", secret_name, secret_value);
		free((void*) secret_value);
	}
	LOCK_ONE(keyring);
}

void display_secrets(const gchar* keyring, const gchar ** secret_names) {
	int i = 0;
	const gchar *secret_value;
	const gchar* secret_name = secret_names[0];
	keyring = check_keyring(keyring);
	if (!is_keyring_existing(keyring)) {
		fprintf(stderr,
				"Warning %s is not a keyring, the default keyring will be used.\n",
				keyring);
		keyring = NULL;
	}
	UNLOCK_ONE(keyring);
	while (secret_name != NULL
			&& (keyring == NULL || secret_names[i + 1] != NULL)) {
		secret_value = get_secret_value(keyring, secret_names[i]);
		printf("%s = %s\n", secret_names[i], secret_value);
		free((void*) secret_value);
		secret_name = secret_names[++i];
	}
	LOCK_ONE(keyring);
	free(keyring);
}

void copy_secret_value_to_clipboard(const gchar * keyring,
		const gchar * secret_name) {
	keyring = check_keyring(keyring);
	UNLOCK_ONE(keyring);
	const gchar *secret_value = get_secret_value(keyring, secret_name);
	if (secret_value != NULL) {
		gtk_init(NULL, NULL);
		GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
		gtk_clipboard_set_text(clipboard, secret_value, -1);
//		printf("%s\n", gtk_clipboard_wait_for_text(clipboard));
		printf("The %s's secret value is copied in gnome clipboard.\n",
				secret_name);
		gtk_clipboard_store(clipboard);
		free((void*) secret_value);
	}
	LOCK_ONE(keyring);
	free(keyring);
}

void copy_secret_value_to_clipboard_tmp(const gchar * keyring,
		const gchar * secret_name) {
	copy_secret_value_to_clipboard(keyring, secret_name);
	printf(
			"The %s's secret value will be cleared from clipboard in 60 seconds.\n",
			secret_name);
	sleep(60);
	GtkClipboard *clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_text(clipboard, "", -1);
	printf("The %s's secret value cleared from clipboard.\n", secret_name);
	gtk_clipboard_store(clipboard);
}

void add_secret(const gchar * keyring, const gchar * secret_name) {
	set_secret(keyring, secret_name, TRUE);
}

void set_secret(const gchar *keyring, const gchar *secret_name, gboolean adding) {
	keyring = check_keyring(keyring);
	UNLOCK_ONE(keyring);
	gchar* sec_existing = get_secret_value(keyring, secret_name);
	if (adding || sec_existing) {
		if (sec_existing) {
			gchar resp[64];
			while (strcmp("yes", resp) && strcmp("no", resp)) {
				printf("Secret %s is already set in keyring %s.\n"
						"Are you sure you want to override its value?",
						secret_name, keyring);
				printf(" [yes/no]\n");
				bzero(resp, sizeof(resp));
				scanf("%s", resp);
				//swallow garbage lf
				getchar();
			}
			if (!strcmp("no", resp)) {
				LOCK_ONE(keyring);
				free(keyring);
				return;
			}
		}
		gchar* secret = get_not_echoed_user_input(secret_name);
		GnomeKeyringResult r = gnome_keyring_store_password_sync(&sec_schema,
				keyring, secret_name, (const gchar *) secret, "id", secret_name,
				NULL);
		if (GNOME_KEYRING_RESULT_OK == r)
			printf("Secret %s is now in keyring %s.\n", secret_name, keyring);
		else
			fprintf(stderr, "Error adding secret to keyring : %s.\n",
					gnome_keyring_result_to_message(r));
		free(secret);
	}
	LOCK_ONE(keyring);
	free(keyring);
}

void delete_secret(const gchar * keyring, const gchar * secret_name) {
	UNLOCK_ONE(keyring);
	GnomeKeyringResult r;
	keyring = check_keyring(keyring);
	guint32 id = find_secret(keyring, secret_name, NULL);
	if (id > 0) {
		r = gnome_keyring_item_delete_sync(keyring, id);
		if (GNOME_KEYRING_RESULT_OK == r)
			printf("Secret %s deleted from keyring %s.\n", secret_name,
					keyring);
		else
			fprintf(stderr, "Error deleting secret in keyring : %s.\n",
					gnome_keyring_result_to_message(r));
	}
	LOCK_ONE(keyring);
	free(keyring);
}

void list_secrets(const gchar * keyring) {
	keyring = check_keyring(keyring);
	UNLOCK_ONE(keyring);
	GList * ids;
	gnome_keyring_list_item_ids_sync(keyring, &ids);
	GList * cur = ids;
	uint id;
	GnomeKeyringResult r;
	GnomeKeyringItemInfo * info;
	while (cur != NULL) {
		id = GPOINTER_TO_UINT(cur->data);
		printf("id = %u, ", id);
		r = gnome_keyring_item_get_info_sync(keyring, id, &info);
		if (GNOME_KEYRING_RESULT_OK != r)
			fprintf(stderr, "Error getting info for id = %u in keyring : %s.\n",
					id, gnome_keyring_result_to_message(r));
		else
			printf("name = %s\n",
					gnome_keyring_item_info_get_display_name(info));
		gnome_keyring_item_info_free(info);
		cur = cur->next;
	}
	g_list_free(ids);
	LOCK_ONE(keyring);
	free(keyring);
}

void list_keyrings() {
	GList * keyrings = NULL;
	GnomeKeyringResult r = gnome_keyring_list_keyring_names_sync(&keyrings);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error listing keyrings : %s.\n",
				gnome_keyring_result_to_message(r));
	else {
		printf("List of keyrings :\n");
		while (keyrings != NULL) {
			printf("%s\n", keyrings->data);
			keyrings = keyrings->next;
		}
	}
	if (keyrings != NULL)
		gnome_keyring_string_list_free(keyrings);
}

gboolean is_keyring_existing(const gchar* keyring) {
	GList * keyrings = NULL;
	gboolean keyring_existing = FALSE;
	gchar* tmp;
	GnomeKeyringResult r = gnome_keyring_list_keyring_names_sync(&keyrings);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error listing keyrings : %s.\n",
				gnome_keyring_result_to_message(r));
	else {
		while (keyrings != NULL && !keyring_existing) {
			tmp = keyrings->data;
			if (!strcmp(keyring, tmp))
				keyring_existing = TRUE;
			keyrings = keyrings->next;
		}
	}
	if (keyrings != NULL)
		gnome_keyring_string_list_free(keyrings);
	return keyring_existing;
}

guint count_items(const gchar *keyring) {
	guint count = 0;
	GList* ids;
	GnomeKeyringResult r = gnome_keyring_list_item_ids_sync(keyring, &ids);
	if (r != GNOME_KEYRING_RESULT_OK)
		fprintf(stderr, "Error getting secret : %s.\n",
				gnome_keyring_result_to_message(r));
	else
		while (ids != NULL) {
			ids = ids->next;
			count++;
		}
	return count;
}

void print_props(const gchar *keyring) {
//	UNLOCK_ONE(keyring);
	keyring = check_keyring(keyring);
	GnomeKeyringInfo * info = NULL;
	GnomeKeyringResult r = gnome_keyring_get_info_sync(keyring, &info);
	if (GNOME_KEYRING_RESULT_OK != r) {
		fprintf(stderr, "Error getting keyring = %s properties : %s.\n",
				keyring, gnome_keyring_result_to_message(r));
	} else {
		time_t mt = gnome_keyring_info_get_mtime(info);
		time_t ct = gnome_keyring_info_get_ctime(info);
		gboolean autolock = gnome_keyring_info_get_lock_on_idle(info);
		printf("Gnome keyring = %s properties:\n", keyring);
		printf("Creation time: %s", ctime(&ct));
		printf("Last modification time: %s", ctime(&mt));
		autolock ? printf("Locked ") : printf("Not locked ");
		printf("if idle.\n");
		printf("Number of items : %u.\n", count_items(keyring));
	}
	gnome_keyring_info_free(info);
//	LOCK_ONE(keyring);
	free(keyring);
}

void set_default_keyring(const gchar *keyring) {
	gnome_keyring_set_default_keyring_sync(keyring);
}

GnomeKeyringResult create(const gchar *keyring) {
	GnomeKeyringResult r = gnome_keyring_create_sync(keyring, NULL);
	if (GNOME_KEYRING_RESULT_OK != r)
		print_keyring_error(keyring, r, "Error creating keyring = %s : %s.\n",
				FALSE);
	else
		printf("keyring %s is created.\n", keyring);
	lock(keyring);
	return r;
}

void change_password(const gchar * keyring) {
	keyring = check_keyring(keyring);
	GnomeKeyringResult r = gnome_keyring_change_password_sync(keyring, NULL,
	NULL);
	if (GNOME_KEYRING_RESULT_OK != r)
		fprintf(stderr, "Error changing password on keyring = %s : %s.\n",
				keyring, gnome_keyring_result_to_message(r));
	else
		printf("Password has successfully been changed on keyring %s .\n",
				keyring);
	free(keyring);
}

void diff(const gchar* keyring1, const gchar* keyring2) {
	UNLOCK_TWO(keyring1, keyring2)
	diff_res dr = { 0 };
	diff_dr(keyring1, keyring2, &dr);
	print_diff_res(&dr, keyring1, keyring2);
// don't lock a before unlocked keyring
	LOCK_TWO(keyring1, keyring2);
}

void merge(const gchar* keyring1, const gchar* keyring2,
		const gchar* out_keyring) {
	guint count;
	UNLOCK_TWO(keyring1, keyring2);
	GnomeKeyringResult r;
// try creating out_keyring
	r = create(out_keyring);
	printf("Trying to merge...\n");
	if (r == GNOME_KEYRING_RESULT_OK) {
		diff_res dr = { 0 };
		// diff keyring1, keyring2
		diff_dr(keyring1, keyring2, &dr);
		// copy diff subsets in out_keyring (asking user to choose between concurrent secret values)
		count = copy_dr_secrets(out_keyring, &dr);
		printf("%u secrets have been merged in %s.\n", count, out_keyring);
		lock(out_keyring);
	}
// don't lock a before unlocked keyring
	LOCK_TWO(keyring1, keyring2);
}

void usage(const char * prog_name) {
	printf("%s add <secret> [<keyring>]\n", prog_name);
	printf("%s set <secret> [<keyring>]\n", prog_name);
	printf("%s display <secret1> [<secret2>[...[<secretN>]]] [<keyring>]\n",
			prog_name);
	printf("%s delete <secret> [<keyring>]\n", prog_name);
	printf("%s list [<keyring>]\n", prog_name);
	printf("%s list_keyrings\n", prog_name);
	printf("%s lock [<keyring> [<keyring2>[ ...[<keyringN>]]]]\n", prog_name);
	printf("%s lock_all\n", prog_name);
	printf("%s unlock [<keyring> [<keyring2>[ ...[<keyringN>]]]]\n", prog_name);
	printf("%s create <keyring>\n", prog_name);
	printf("%s diff <keyring1> <keyring2>\n", prog_name);
	printf("%s merge <keyring1> <keyring2> <out_keyring>\n", prog_name);
	printf("%s copy <secret> [<keyring>]\n", prog_name);
	printf("%s copy_tmp <secret> [<keyring>]\n", prog_name);
	printf("%s change_pass [<keyring>]\n", prog_name);
	printf("%s props [<keyring>]\n", prog_name);
	printf("%s set_default <keyring>\n", prog_name);
//TODO : unhide when gnome-keyring will really implement gnome_keyring_info_set_lock_on_idle()/set_lock_timeout().
//	printf("%s enable_autolock [<keyring> [<timeout_secs>]]\n", prog_name);
//	printf("%s disable_autolock [<keyring>]\n", prog_name);
}

void enable_autolock(int argc, char** argv) {
	guint32 timeout;
	if (argc > 3)
		timeout = atoi(argv[3]);
	else
		timeout = DEFAULT_AUTOLOCK_TIMEOUT;
	set_auto_lock(argc > 2 ? argv[2] : NULL, TRUE, timeout);
}

int main(int argc, char ** argv) {
	char * argv3 = NULL;
	if (!gnome_keyring_is_available()) {
		fprintf(stderr, "Error: gnome keyring daemon is not available.\n");
		return EXIT_FAILURE;
	}
	if (argc > 1 && strlen(argv[1]) < 16) {
		if (argc > 3)
			argv3 = argv[3];
		if (!strcmp(argv[1], "add") && argc > 3)
			add_secret(argv3, argv[2]);
		else if (!strcmp(argv[1], "set") && argc > 3)
			set_secret(argv3, argv[2], FALSE);
		else if (!strcmp(argv[1], "display") && argc > 2)
			display_secrets(argv[argc - 1], (const gchar**) &argv[2]);
		else if (!strcmp(argv[1], "delete") && argc > 3)
			delete_secret(argv3, argv[2]);
		else if (!strcmp(argv[1], "list") && argc > 1)
			list_secrets(argc > 2 ? argv[2] : NULL);
		else if (!strcmp(argv[1], "list_keyrings") && argc > 1)
			list_keyrings();
		else if (!strcmp(argv[1], "lock") && argc > 1)
			if (argc > 2)
				multi_lock((const gchar**) (argv + 2), argc - 2);
			else
				lock(NULL);
		else if (!strcmp(argv[1], "lock_all") && argc > 1)
			lock_all();
		else if (!strcmp(argv[1], "unlock") && argc > 1)
			if (argc > 2)
				multi_unlock((const gchar**) (argv + 2), argc - 2);
			else
				unlock(NULL);
		else if (!strcmp(argv[1], "set_default") && argc > 2)
			set_default_keyring(argv[2]);
		else if (!strcmp(argv[1], "create") && argc > 2)
			create(argv[2]);
		else if (!strcmp(argv[1], "change_pass") && argc > 1)
			change_password(argc > 2 ? argv[2] : NULL);
		else if (!strcmp(argv[1], "props") && argc > 1)
			print_props(argc > 2 ? argv[2] : NULL);
		else if (!strcmp(argv[1], "diff") && argc > 3)
			diff(argv[2], argv[3]);
		else if (!strcmp(argv[1], "merge") && argc > 4)
			merge(argv[2], argv[3], argv[4]);
		else if (!strcmp(argv[1], "copy") && argc > 3)
			copy_secret_value_to_clipboard(argv3, argv[2]);
		else if (!strcmp(argv[1], "copy_tmp") && argc > 3)
			copy_secret_value_to_clipboard_tmp(argv3, argv[2]);
		else if (!strcmp(argv[1], "enable_autolock") && argc > 1) {
			enable_autolock(argc, argv);
		} else if (!strcmp(argv[1], "disable_autolock") && argc > 1) {
			set_auto_lock(argc > 2 ? argv[2] : NULL, FALSE, 0);
		} else
			usage(argv[0]);
	} else
		usage(argv[0]);
//TODO : don't return SUCCESS in all cases
	return EXIT_SUCCESS;
}
