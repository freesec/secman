
What is secman?  
===============  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="secman.png" width="128" />  
  
Secman is the shortname for Secret Manager. This is a command-line tool to manage your passwords and other secret texts. It is based on the gnome-keyring cryptographic solution, hence it can't be used without gnome (or at least the gnome-keyring library and D-Bus).  
A keyring is a set of secrets that you can unlock with a passphrase in order to consult, add or edit the secrets. 
It's an essential tool nowadays when we create dozens of accounts and can't afford to remember all the different secure passwords we use for them.  
Plenty of softwares already do that, this one is oriented to be used programmatically through scripts or interactively on the command-line.
  
  
How to install  
==============  
  
Up to now the project is only available through the source code, so you'll need to compile it.  
The project uses autotools to handle the dependencies and the compiling environment.  
  
Normally you'll only need to type these commands for compiling the project:

	./configure  
	make  
	sudo make install  
	# if something went wrong, please look at the troubleshooting section below

Usage
======

Typing ``secman`` you'll get the command summary. Below you'll find a description of each command.  
The first thing you have to do is to ``create`` a keyring but normally the ``login`` keyring of gnome is already set as the default keyring in your environment, but here is the thing: you can create several keyrings for different purposes. Then you can ``add``, ``set`` and ``display`` the secrets of your keyrings.  
A bunch of commands are available and more advanced features too, just read below.  

	$ secman 
	secman add <secret> [<keyring>]
	secman set <secret> [<keyring>]
	secman display <secret1> [<secret2>[...[<secretN>]]] [<keyring>]
	secman delete <secret> [<keyring>]
	secman list [<keyring>]
	secman list_keyrings
	secman lock [<keyring> [<keyring2>[ ...[<keyringN>]]]]
	secman lock_all
	secman unlock [<keyring> [<keyring2>[ ...[<keyringN>]]]]
	secman create <keyring>
	secman diff <keyring1> <keyring2>
	secman merge <keyring1> <keyring2> <out_keyring>
	secman copy <secret> [<keyring>]
	secman copy_tmp <secret> [<keyring>]
	secman change_pass [<keyring>]
	secman props [<keyring>]
	secman set_default <keyring>

Nota: in the command list above, the brackets `[`, `]` represent an optional argument or list of arguments if other brackets are enclosed into them. The chevron characters `<`, `>` designate a value argument to specify.

``secman add <secret> [<keyring>]``: adds the secret named ``secret`` in the keyring named ``keyring`` if specified or the default keyring otherwise.  
``secman set <secret> [<keyring>]``: edits the secret named ``secret`` in the keyring named ``keyring`` if specified or the default keyring otherwise. _Warning_: this command erase older secret value.  
``secman display <secret1> [<secret2>[...[<secretN>]]] [<keyring>]``: displays the value of the secret named ``secret`` located in the keyring named ``keyring`` if specified or the default keyring otherwise.  
``secman delete <secret> [<keyring>]``: deletes the secret named ``secret`` and located in the keyring named ``keyring`` if specified or the default keyring otherwise.  
``secman list [<keyring>]``: lists all secrets hosted by the keyring named ``keyring`` if specified or the default keyring otherwise.  
``secman list_keyrings``: lists all keyrings available for the current user.  
``secman lock [<keyring> [<keyring2>[ ...[<keyringN>]]]]``: lock all the keyring specified in the list or only the default keyring if the list is empty. It means the user will have to give the passphrase to access again the keyring(s).   
``secman unlock [<keyring> [<keyring2>[ ...[<keyringN>]]]]``: unlock all the kerying specified in the list or only the default keyring if the list is empty. The passphrase will be asked in order to unlock the keyrings.  
``secman change_pass [<keyring>]``: changes the passphrase of the keyring ``keyring`` or the default keyring if not specified.  
``secman props [<keyring>]``: displays properties of the keyring ``keyring`` or the default keyring if not specified. Among the properties is the number of items/secrets in the keyring.  
``secman set_default <keyring>``: set the keyring ``keyring`` as the default keyring for the current user. When any of the commands except few ones are used without specifiying a keyring, the default keyring is used.  


Advanced commands/features
==========================

``secman diff <keyring1> <keyring2>``: lists the differences between two keyrings. All the secrets found in both keyrings are separated in four subsets: 1) the secrets found in only keyring1, 2) the secrets found in only keyring2, 3) the secrets found in both keyrings and being equal, 4) the secrets found in both keyrings and storing different values.  
``secman merge <keyring1> <keyring2> <out_keyring>``: merges the keyrings ``keyring1`` and ``keyring2`` into the new keyring ``out_keyring``. When a secret name is the same for both keyrings, the user is asked to choose which one to copy in ``out_keyring``. This command is very handy when you have copied a keyring elsewhere and made it evolve while the original one has continued to evolve too (that normally shouldn't happen but you know what real life is, anyway this command will save your day).  
``secman copy <secret> [<keyring>]``: copies in the clipboard the secret ``secret`` from keyring ``keyring`` if specified or the default keyring otherwise. Useful to copy/paste a secret. Nota: its seems to fail when running wayland.  
``secman copy_tmp <secret> [<keyring>]``: does the same as command ``copy`` but here the clipboard is washed up automatically after 60 seconds.  

Script example
==============

	#!/bin/bash
	# This script is an example of how you can benefit to use secman in a script
	# Here we retrieve the user-password pair secret from a keyring and make the connection to gitlab through a proxy which needs authentication
	# Obviously it's more secure to do like this than entering the password as clear text in the script

	LOGIN='my_proxy_login' # the secret name
	KEYRING='my_keyring' # the keyring that contains the secret
	PROXY=proxy_hostname.com
	PROXY_PORT=3128

	# Use secman to get the password
	PASS=$(secman display $LOGIN $KEYRING | sed -e 's/.*= //')

	# make the HTTP request through the proxy
	curl -U "$LOGIN:$PASS" --proxy https://$PROXY:$PROXY_PORT https://gitlab.com
	##################################################

Troubleshooting
===============

- If the ``configure`` command fails somewhere on the road, it could be helpful to regenerate the configure script with the command ``autoreconf -i``. For that you'll need to install autoconf, automake and m4.
- If you got the following error when running configure:

	checking for DEPS... no
	configure: error: Package requirements (gnome-keyring-1 gtk+-3.0) were not met:

	Package 'gnome-keyring-1', required by 'virtual:world', not found
	Package 'gtk+-3.0', required by 'virtual:world', not found


	It's quite obvious you need to install the depencencies libgnome-keyring, libgnome-keyring-dev(el), gtk3 and gtk3-dev(el).


